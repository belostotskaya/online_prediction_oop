#ifndef ONLINE_PREDICTION_FRAMEWORK_H_
#define ONLINE_PREDICTION_FRAMEWORK_H_

#include <vector>
using std::vector;

// Природа
class Nature {
public:
  // Возвращает исход очередного раунда.
  virtual double omega() = 0;
};

// Функция потерь. Первый принимаемый параметр принадлежит пространству исходов,
// второй - пространству предсказаний.
typedef double(LambdaFunction)(double, double);

// Статистик
class Statistician {
public:
  // Возвращает предсказание статистика в очередном раунде при заданных
  // предсказаниях экспертов.
  virtual double gamma(vector<double> const & ksi) = 0;
};

// Эксперт
class Expert {
public:
  // Возвращает предсказание эксперта в очередном раунде.
  virtual double ksi() = 0;
};

// Интерфейс, выражающий возможность знать об исходах Природы.
class AwareAboutNatureOutcome {
public:
  // Уведомление участника игры об исходе природы.
  virtual void ack_omega(double omega) = 0;
};

// Симулирует t раундов взаимодействия Природы, Статистика и Экспертов. На
// каждом ходу эксперты объявляют свои предсказания; Статистик делаем свое,
// узнав ответы экспертов; Природа объявляет исход; Статистик и Эксперты терпят
// потери в соответствии с функцей lambda. Записывает потери Статистика и
// Экспертов в вектора stat_loses и exp_loses соответсвенно.
void play_online_prediction_game(
    int t,
    Nature * nat,
    LambdaFunction lambda,
    Statistician * stat,
    vector<Expert *> exp,
    vector<double> & stat_losses,
    vector<vector<double>> & exp_losses);

// Делает то же, что и предыдущая функция, но не возвращает потери Статистика и
// Экспертов.
void play_online_prediction_game(
    int t,
    Nature * nat,
    LambdaFunction lambda,
    Statistician * stat,
    vector<Expert *> exp);

// Вычисляет регрет для каждого раунда.
vector<double> regret(
    vector<double> const & stat_losses,
    vector<vector<double>> const & exp_losses);

#endif // ONLINE_PREDICTION_FRAMEWORK_H_
