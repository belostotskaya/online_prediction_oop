#ifndef STATISTICIAN_H_
#define STATISTICIAN_H_

#include <stdexcept>

#include "online_prediction_framework.h"
#include "utils.h"

class FindAlwaysRightExpert:
    public Statistician, public AwareAboutNatureOutcome {
private:
  int experts_count;
  vector<double> w;
  vector<double> ksi;

  bool is_valid(double x) {
    return is_equal(x, 0) || is_equal(x, 1);
  }
public:
  FindAlwaysRightExpert(int experts_count):
      experts_count(experts_count), w(experts_count, 1) {
  }

  double gamma(vector<double> const & ksi) {
    if (ksi.size() != experts_count) {
      throw std::runtime_error("Wrong count of experts.");
    }
    for (int i = 0; i < experts_count; ++i) {
      if (!is_valid(ksi[i])) {
        throw std::runtime_error("Predictions of experts must be one of {0, 1}.");
      }
    }

    this->ksi = ksi;
    int cnt[2] = {0, 0};
    for (int i = 0; i < experts_count; ++i) {
      if (is_equal(w[i], 0)) {
        continue;
      }
      ++cnt[is_equal(ksi[i], 0) ? 0 : 1];
    }
    if (cnt[0] + cnt[1] == 0) {
      throw std::runtime_error("There should be always right expert.");
    }
    if (cnt[0] > cnt[1]) {
      return 0;
    } else {
      return 1;
    }
  }

  void ack_omega(double omega) {
    if (!is_valid(omega)) {
      throw std::runtime_error("Outcome must be one of {0, 1}.");
    }
    for (int i = 0; i < experts_count; ++i) {
      if (!is_equal(ksi[i], omega)) {
        w[i] = 0;
      }
    }
  }
};

#endif // STATISTICIAN_H_
