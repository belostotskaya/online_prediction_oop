#ifndef EXPERTS_H_
#define EXPERTS_H_

#include "online_prediction_framework.h"

class ExpertWithExternalAdvice: public Expert {
private:
  bool advice_was_provided;
  double advice;
  Expert * expert;
public:
  ExpertWithExternalAdvice(Expert * expert): expert(expert) {
  }

  void provide_next_round_advice(double advice) {
    advice_was_provided = true;
    this->advice = advice;
  }

  virtual double ksi() {
    double ksi = expert->ksi();
    if (advice_was_provided) {
      advice_was_provided = false;
      return advice;
    } else {
      return ksi;
    }
  }
};

#endif // EXPERTS_H_
