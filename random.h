#ifndef RANDOM_H_
#define RANDOM_H_

#include "online_prediction_framework.h"

// Generates random number from a to b inclusive. a should be less or equals to
// b.
double get_random(double a, double b);

class RandomNature: public Nature {
public:
  double omega() {
    return get_random(0, 1);
  }
};

class Random01Nature: public Nature {
public:
  double omega() {
    return (get_random(0, 1) > 0.5) ? 1 : 0;
  }
};

class RandomStatistician: public Statistician {
public:
  double gamma(vector<double> const & ksi) {
    return get_random(0, 1);
  }
};

class RandomExpert: public Expert {
public:
  double ksi() {
    return get_random(0, 1);
  }
};

class Random01Expert: public Expert {
public:
  double ksi() {
    return (get_random(0, 1) > 0.5) ? 1 : 0;
  }
};

#endif // RANDOM_H_
