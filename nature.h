#ifndef NATURE_H_
#define NATURE_H_

#include <vector>

#include "online_prediction_framework.h"
#include "experts.h"

using std::vector;

// Декоратор, уведомляющий других участников игры об исходе природы, в конце
// каждого раунда.
class NotifyAfterOutcome: public Nature {
private:
  Nature * nat;
  vector<AwareAboutNatureOutcome *> players;
public:
  NotifyAfterOutcome(Nature * nat): nat(nat) {
  }

  NotifyAfterOutcome * forPlayer(
    AwareAboutNatureOutcome * player) {
    this->players.push_back(player);
    return this;
  }

  virtual double omega() {
    double omega = nat->omega();
    for (int i = 0; i < (int)players.size(); ++i) {
      players[i]->ack_omega(omega);
    }
    return omega;
  }
};

// Декоратор, уведомляющий экспертов об исходе природы до того, как она объявила
// его в очередном раунде.
class NotifyExpertsBeforeOutcome: public Nature {
private:
  Nature * nat;
  vector<ExpertWithExternalAdvice *> experts;
  double next_omega;

  void notify_experts() {
    for (int i = 0; i < (int)experts.size(); ++i) {
      experts[i]->provide_next_round_advice(next_omega);
    }
  }
public:
  NotifyExpertsBeforeOutcome(Nature * nat): nat(nat) {
    next_omega = nat->omega();
  }

  NotifyExpertsBeforeOutcome * forExpert(
    ExpertWithExternalAdvice * expert) {
    experts.push_back(expert);
    expert->provide_next_round_advice(next_omega);
    return this;
  }

  virtual double omega() {
    double omega = next_omega;
    next_omega = nat->omega();
    notify_experts();
    return omega;
  }
};

#endif // NATURE_H_
