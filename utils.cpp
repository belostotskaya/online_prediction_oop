#include "utils.h"

#include <cmath>
#include <limits>

bool is_equal(double a, double b) {
  return fabs(b - a) <= 1e-9;
  // return fabs(b - a) <= std::numeric_limits<double>::epsilon();
}
