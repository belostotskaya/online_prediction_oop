#ifndef UTILS_H_
#define UTILS_H_

#include <iostream>
#include <vector>

template <typename T>
std::ostream & operator << (std::ostream & out, std::vector<T> const & a) {
  out << "[";
  for (int i = 0; i < (int)a.size(); ++i) {
    if (i != 0) {
      out << ", ";
    }
    out << a[i];
  }
  out << "]";
  return out;
}

bool is_equal(double a, double b);

#endif // UTILS_H_
