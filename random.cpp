#include "random.h"

#include <stdexcept>
#include <random>

double get_random(double a, double b) {
  if (a > b) {
    throw std::runtime_error(
      "a should be less or equals to b in get_random");
  }
  static std::mt19937 gen(42);
  return std::uniform_real_distribution<>(a, b)(gen);
}
