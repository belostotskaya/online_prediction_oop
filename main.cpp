#include <vector>
#include <iostream>
#include <algorithm>

#include "online_prediction_framework.h"
#include "random.h"
#include "nature.h"
#include "statistician.h"
#include "experts.h"
#include "utils.h"

using namespace std;

double qudratic_loss(double omega, double gamma) {
  return (omega - gamma) * (omega - gamma);
}

int main() {
  int experts_count = 100;
  vector<Expert *> experts;
  for (int i = 1; i < experts_count; ++i) {
    experts.push_back(new Random01Expert());
  }
  auto always_right_expert = new ExpertWithExternalAdvice(new Random01Expert());
  experts.push_back(always_right_expert);
  random_shuffle(experts.begin(), experts.end());
  auto * statistician = new FindAlwaysRightExpert(experts_count);

  Nature * nature = new Random01Nature();
  NotifyExpertsBeforeOutcome * notify_experts_before_outcome =
    new NotifyExpertsBeforeOutcome(nature);
  nature = notify_experts_before_outcome;
  NotifyAfterOutcome * notify_after_outcome = new NotifyAfterOutcome(nature);
  nature = notify_after_outcome;
  notify_experts_before_outcome->forExpert(always_right_expert);
  notify_after_outcome->forPlayer(statistician);

  vector<double> stat_losses;
  vector<vector<double>> exp_losses;
  play_online_prediction_game(
    10, nature, qudratic_loss, statistician, experts,
    stat_losses, exp_losses);
  vector<double> regret = ::regret(stat_losses, exp_losses);
  cout << "Regret: " << regret << "\n";
  return 0;
}
