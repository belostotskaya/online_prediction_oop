#include "online_prediction_framework.h"

#include <iostream>
#include <stdexcept>

#include "utils.h"

using namespace std;

void play_online_prediction_game(
    int t,
    Nature * nat,
    LambdaFunction lambda,
    Statistician * stat,
    vector<Expert *> exp) {
  vector<double> stat_loses;
  vector<vector<double>> exp_loses;
  play_online_prediction_game(t, nat, lambda, stat, exp, stat_loses, exp_loses);
}

void play_online_prediction_game(
    int t,
    Nature * nat,
    LambdaFunction lambda,
    Statistician * stat,
    vector<Expert *> exp,
    vector<double> & stat_losses,
    vector<vector<double>> & exp_losses) {
  cout << "Play prediction game with " << t << " rounds\n";

  stat_losses = {};
  exp_losses = {};
  double total_loss = 0;
  for (int i = 0; i < t; ++i) {
    cout << "\n";
    cout << "Round #" << i + 1 << ":\n";

    vector<double> ksi;
    for (int j = 0; j < (int)exp.size(); ++j) {
      ksi.push_back(exp[j]->ksi());
    }
    cout << "Expert's predictions: " << ksi << "\n";
    double gamma = stat->gamma(ksi);
    double omega = nat->omega();
    cout << "Statistician prediction: " << gamma << "\n";
    cout << "Nature prediction: " << omega << "\n";

    double loss = lambda(omega, gamma);
    stat_losses.push_back(loss);
    total_loss += loss;
    vector<double> current_exp_losses;
    for (int j = 0; j < (int)exp.size(); ++j) {
      current_exp_losses.push_back(lambda(omega, ksi[j]));
    }
    exp_losses.push_back(current_exp_losses);
    cout << "Loss of experts: " << current_exp_losses << "\n";
    cout << "Loss of the statistician: " << loss << "\n";
  }
  cout << "\n";
  cout << "Total loss of statistician: " << total_loss << "\n";
}

vector<double> regret(
    vector<double> const & stat_losses,
    vector<vector<double>> const & exp_losses) {
  if (stat_losses.size() != exp_losses.size()) {
    throw std::runtime_error("Size of stat_losses and exp_losses should be "
      "the same.");
  }
  int t = stat_losses.size();
  if (t == 0) {
    return {};
  }
  int k = exp_losses[0].size();

  vector<double> regret;
  double stat_total_loss = 0;
  vector<double> exp_total_losses(k, 0);
  for (int i = 0; i < t; ++i) {
    stat_total_loss += stat_losses[i];
    if (exp_losses[i].size() != k) {
      throw std::runtime_error("Sizes of vectors in exp_losses should be "
        "the same.");
    }
    for (int j = 0; j < k; ++j) {
      exp_total_losses[j] += exp_losses[i][j];
    }
    int mn = 0;
    for (int j = 1; j < k; ++j) {
      if (exp_total_losses[j] < exp_total_losses[mn]) {
        mn = j;
      }
    }
    regret.push_back(stat_total_loss - exp_total_losses[mn]);
  }
  return regret;
}
